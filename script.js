// This function clear all the values
var opflag = 0;
var npflag = 1;
var bracketflag = 0;
function clearScreen() {
	 document.getElementById("result").value = "";
	 document.getElementById("answer").value = "";
	 opflag = 0;
	 npflag = 1;
	 bracketflag = 0;
}

function Del() {
	 var res = document.getElementById("result").value;
	 var ans = res.substring(0,res.length-1);
	 if(checkOperator(res.substring(res.length-1,1)))
	 	opflag--;
	 document.getElementById("result").value = ans;
}

// This function display values
function display(value) {
	opflag=0;
	npflag=0;
	 document.getElementById("result").value += value;
}
// This function evaluates the expression and return result

function calculate() {
	if(opflag !=0 && bracketflag==0){
		myalert('check! incorrect');
	}
	if(document.getElementById("Degree").checked)
	 	document.getElementById("answer").value = parse();
	else
		document.getElementById("answer").value = parse();
}

function operator(op){
	if(opflag >0 ){
		myalert('input error!')
		return;
	}
	npflag = 1;
	opflag=1;
	var p = document.getElementById("result").value;
	
	document.getElementById("result").value =p+op;
}

function checkOperator(op){
	if(op=='-'||op=='+'||op=='*'||op=='/'){
		return true;
	}
	return false;
}

function op_bracket(op){
	if(npflag==0){
		myalert('input error!')
		return;
	}
	opflag=0;

	var p = document.getElementById("result").value;
	document.getElementById("result").value = p+op;
	bracketflag++;
}


function closebracket(op){
	
	var p = document.getElementById("result").value;
	document.getElementById("result").value =p + op;

	bracketflag--;
}

function parse(){
	
	var p = document.getElementById("result").value;
	var openid = false;
	var last_appearBracket=0;
	for(var i=0;i<p.length;i++){
		var pt = p.charAt(i);
		if(pt=='(')
			last_appearBracket = i;
		else if(pt==')'&&!checkOperator(p.charAt(i-1)))
		{
			
			p=calcSpec(last_appearBracket,p,i);
			debugger;
			p=checkValidateOperator(p);
			i=0;
		}
	}
	return eval(p);
}
function checkValidateOperator(str){
	for(var i=0;i<str.length;i++){
		var ch = str.charAt(i);
		var och = str.charAt(i+1);
		if(checkOperator(ch)&&checkOperator(och))
		{
			debugger;
			var front = str.substr(0,i+1)+'0'+str.substr(i+1,str.length-i+1);
			return front;
		}
	}
	debugger;
	return str;

}
function calcSpec(index,str,last){
	if(str.substr(index-3,3) == "sin"){
		var orign = str.substr(index-3,last-(index-4))
		var res = str.substr(index+1,last-index-1);
		return str.replace(orign,Math.sin(checkState(eval(res))));
	}
	else if(str.substr(index-3,3) == "cos"){
		var orign = str.substr(index-3,last-(index-4))
		var res = str.substr(index+1,last-index-1);
		return str.replace(orign,Math.cos(checkState(eval(res))));
	}
	
	else if(str.substr(index-3,3)== "tan"){
		var orign = str.substr(index-3,last-(index-4))
		var res = str.substr(index+1,last-index-1);
		return str.replace(orign,Math.tan(checkState(eval(res))));
	}

	else if(str.substr(index-3,3) == "log"){
		var orign = str.substr(index-3,last-(index-4))
		var res = str.substr(index+1,last-index-1);
		return str.replace(orign,Math.log(checkState(eval(res))));
	}
	else if(str.substr(index-5,5) =="reSin"){
		var res = str.substr(index+1,last-index-1);
		var orign = str.substr(index-5,last-(index-6));
		return str.replace(orign,eval(res)*392.9);
	}
	else {
		var res = str.substr(index+1,last-index-1);
		var orign = str.substr(index,last);
		return str.replace(orign,res);
	}
}

function myalert(txt){
	document.getElementById("answer").value = txt;
	document.getElementById("answer").style.borderColor = "red"; 
}

function checkState(val){
	if(document.getElementById("Degree").checked)
		return val / 360 * 2 * Math.PI
	else 
		return val;
}
